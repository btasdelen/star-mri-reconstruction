# References:

## gridding: 

http://www-mrsrl.stanford.edu/~brian/gridding/


## arrShow: 

https://github.com/tsumpf/arrShow.git

T.J. Sumpf and M. Untenberger, "arrayShow: a guide to an open source Matlab tool for complex MRI data analysis," Proc. Int. Soc. Mag. Reson. Med., vol. 21, p. 3719, 2013.

## mapVBVD:

https://github.com/CIC-methods/FID-A/tree/master/inputOutput/mapVBVD
