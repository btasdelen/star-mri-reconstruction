function w = RhoFilter(sample_number, line_number, dr, isSingleSided)
%RHOFILTER Summary of this function goes here
%   Detailed explanation goes here
    wc = 2*pi*(dr^2)/line_number;

    if isSingleSided == true
        w = abs(wc*(0:sample_number-1));
        w(1) = wc/8;
    else
        w = abs(wc*linspace(-(sample_number/2-1), (sample_number/2-1), sample_number));
    end
    w = repmat(w, line_number, 1);    
end
