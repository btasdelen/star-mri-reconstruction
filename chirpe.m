function rf_chirp_samples = chirpe(N, BW, T)
dt = T/N; % sec

t = 0;

rf_chirp_samples = zeros(N,1);

for step=1:N

    phi_t = 2*pi*BW*((T-t)/(2*T))*t;
    phi_t = phi_t - floor(phi_t/(2*pi))*2*pi;
		
    %phi_t = (phi_t >= (PIx2 - 1e-7)) ? 0 : phi_t;
     if phi_t >= (2*pi - 1e-7) || phi_t < 0
         phi_t = 0;
     end
    t = t + dt;
    rf_chirp_samples(step) = cos(phi_t) + 1j*sin(phi_t);
    
end
% t_a = 0:dt:(T - dt);
% figure()
% subplot(211)
% plot(t_a, abs(rf_chirp_samples))
% subplot(212)
% plot(t_a, angle(rf_chirp_samples))
% 
% 
% rfft = fft(rf_chirp_samples);
% figure()
% plot(np.abs(rfft))
end
