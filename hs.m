BW = 1280/2; % Hz
Emp = 6;
dur = 100e-3; % sec
N = 8192;
t = linspace(-dur/2, dur/2, N);
Arg_t = (pi*BW*t*1e3)./(Emp*N);
% Mag_t = 2./(exp(Arg_t) + exp(-Arg_t));
Mag_t = sech(Arg_t);
Phase_t = tan(Emp*log(Mag_t));
w1 = Mag_t.*exp(1i*Phase_t);
figure;
subplot(211)
plot(t, abs(w1))
subplot(212)
plot(t, angle(w1))
title('w_1(t)')

dt = dur/N;
fmax = 1/dt/2; % kHz
f = linspace(-fmax, fmax, N);  

figure;
plot(f*1e3, abs(fftshift(fft(w1))))
title('W_1(f)')
xlabel('kHz')
