function h = BullseyeRemoval(h, per_part, is_inv)
%BULLSEYEREMOVAL Summary of this function goes here
%   Detailed explanation goes here
H = fft(h);
sample_number = size(H, 1);
wdw = hamming(sample_number*2).^30;
wdw = wdw(sample_number+1:end);

if per_part == false
    H_bar = mean(H, [2 3]);
else
    H_bar = mean(H, 2);
end

H_dbar = fft(ifft(H_bar).*wdw);
H_crf = H_dbar./H_bar;
if is_inv == false
    H_corr = H.*H_crf;
else
    H_corr = H./H_crf;
end
h = ifft(H_corr);
end

