% clear
addpath('gridding/')
addpath('../ReadRaw/mapVBVD')
%% Reconstruction Parameters
exp_date = '200319';
meas_id = 'meas_MID45_CEA_ICE_new_prep_FID95388';

gridded_res = 256;
show_projections = false;
do_leak_subt = false;
correct_grad_delay = false;
bullseye_per_part = false;
discard_first_samples = 2;

%% Load and Prepare Raw Data
folder_path = ['../ReadRaw/exp' exp_date];
file_name = [meas_id '.dat'];

[scan, meas_header, baseline] = ReadAveraged(folder_path, file_name);
if ndims(scan) == 4
   ch2_scan = squeeze(scan(:,1,:,:));
   scan = squeeze(scan(:,2,:,:));
end
[sample_number, line_number, partition_number] = size(scan);

%% Preparation of the DCF and convolution kernel
w1 = rfshape(sample_number, meas_header, true);
% mean_ch2 = mean(ch2_scan, [2 3]);
% mean_ch2(129:end) = 0;
% mean_ch2 = mean_ch2./max(abs(mean_ch2));

% w1 = mean_ch2;

scan = scan(discard_first_samples:end,:,:);
w1 = w1(discard_first_samples:end);

sample_number = size(scan,1);

dcf = RhoFilter(sample_number, line_number, meas_header.dkr, true);
dcf = dcf'./max(dcf, [], [1,2]);

k_tra = transpose(CreateRadialTrajectory(sample_number, line_number, true));

%% Estimate Leak and subtract
if do_leak_subt == true
    disp('Estimating and subtracting the leak.')
    addpath('funcs');

    tic
    l_hat = zeros(size(scan));

    a = zeros(2, partition_number);
    phi = zeros(4, partition_number);
    for part=1:partition_number
        [a(:,part), phi(:,part)] = FitResidual(scan(:,:, part), w1, 12);
        l_hat(:, :, part) = repmat(w1.*abs_lin(a(:, part), 1:sample_number)' ...
            .*exp(-1i*sigm(phi(:, part), 1:sample_number))', [1, line_number]);
    end
    
    figure;
    subplot(211); plot(abs(l_hat(:,:,128)));   hold on; plot(abs(mean(scan(:,:,128), 2)))
    subplot(212); plot(angle(l_hat(:,:,128))); hold on; plot(angle(mean(scan(:,:,128), 2)))
    title('Fitted leak and mean scan')
    
%     [X, Y] = meshgrid(1:256, 1:256);
%     figure; plot3(X, Y, abs(scan(:,:,1)))
%     title('Individual scans')
%     a = zeros(2, line_number, partition_number);
%     phi = zeros(4, line_number, partition_number);
%     for line=1:line_number
%         parfor part=1:partition_number
%             [a(:, line, part), phi(:,line,part)] = FitResidual(scan(:,line, part), w1, 20);
%             l_hat(:, line, part) = w1.*abs_lin(a(:, line,  part), 1:sample_number)' ...
%                 .*exp(-1i*sigm(phi(:, line, part), 1:sample_number))';
%         end
%     end
    scan_sub = scan - l_hat;
    toc
    %% Deconvolve and filter measurements
    h = DeconvolveRF(scan_sub, w1, 1e-4, 'wiener');
else
    h = DeconvolveRF(scan, w1, 1e-4, 'wiener');
end

% Normalize projections
h = h - min(abs(h), [], [1,2,3]);
h = h./(max(abs(h), [], [1,2,3]));

% Filter projections
h = BullseyeRemoval(h, bullseye_per_part, false);
h = BullseyeRemoval(h, bullseye_per_part, false);
h = BullseyeRemoval(h, bullseye_per_part, true);

k_tra_corr = repmat(k_tra, [1,1,partition_number]);
dk = abs(k_tra(2, 1));
if correct_grad_delay == true
   shifts = EstimateGradDelay(h);
   for part=1:partition_number
       k_tra_ang = angle(k_tra);
       k_tra_abs = abs(k_tra);
       k_tra_abs = k_tra_abs - dk*(shifts(:,part))';
       k_tra_corr(:,:,part) = k_tra_abs.*exp(1i.*k_tra_ang);
   end
end

%% Gridding of 2D projections
disp('Gridding of the projections started...');
prj_k = zeros(gridded_res, gridded_res, partition_number);

tic
parfor part=1:partition_number
    prj_k(:,:,part) = gridkb2(k_tra_corr(:,:,part), h(:,:,part), dcf, gridded_res, 4, 2);
end
toc

if show_projections == true
    prj2D = ifftshift(ifft2(ifftshift(prj_k)));
    as(prj2D);
end

%% DCF and trajectory along Z direction
k_tra2 = transpose(CreateRadialTrajectory(gridded_res, partition_number, false));

dcf2 = RhoFilter(gridded_res, partition_number, meas_header.dkr, false);
dcf2 = dcf2'./(max(dcf2, [], [1,2]));

reconK = zeros(gridded_res, gridded_res, gridded_res);

disp('Gridding of the partitions started...');
tic
parfor z_idx = 1:gridded_res
    reconK(z_idx,:,:) = gridkb2(k_tra2, squeeze(prj_k(z_idx,:,:)), dcf2, gridded_res, 4, 2);
end
reconVol = ifftshift(ifftn(ifftshift(reconK)));
toc
    
reconNorm = reconVol - min(abs(reconVol), [], [1,2,3]);
reconNorm = reconNorm./(max(abs(reconVol), [], [1,2,3]));

%% Show Image
info_par.correct_grad_delay = correct_grad_delay;
info_par.do_leak_subt = do_leak_subt;
info_par.BW = meas_header.BW;
info_par.bullseye_per_part = bullseye_per_part;

as(reconNorm, 'select', ':,:,128', 'complexSelect', 'm', 'info', info_par);

