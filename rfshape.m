function [w1] = rfshape(sample_number, meas_header, is_half_duration)
%RFSHAPE Summary of this function goes here
%   Detailed explanation goes here

if meas_header.rf_type == 1
    if is_half_duration
%         win = tukeywin((sample_number/2)+3, 0.105);
%         win = win(3:(end-1));
%         win = win(1:2:end);

        win = tukey_win(sample_number + 2, 0.1, 0);
        win = win(2:2:(end-1));
        w1 = [win.*chirpe(sample_number/2, meas_header.BW, meas_header.T/2); ...
              zeros(sample_number/2, 1)];
%           w1 = [tukeywin(sample_number/2, 0.1).* ...
%               chirpe(sample_number/2, meas_header.BW, meas_header.T/2); zeros(sample_number/2, 1)];
    else
        w1 = tukeywin(sample_number, 0.1).*chirpe(sample_number, meas_header.BW, meas_header.T);
    end
elseif meas_header.rf_type == 2
    Emp = 8;
    t = linspace(-T/2, T/2, sample_number);
    Arg_t = (pi*BW*t*1e3)./(Emp*sample_number);
    Mag_t = sech(Arg_t);
    Phase_t = tan(Emp*log(Mag_t));
    w1 = Mag_t.*exp(1i*Phase_t);
    w1 = w1';
end

end

