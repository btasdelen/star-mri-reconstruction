function shifts = EstimateGradDelay(h)
    [n_samp, n_line, n_part] = size(h);
    shifts = zeros(n_line/2, n_part);
    n_points = n_samp/2;

    for line=1:n_line/2
            g_x = squeeze(fftshift(fft(abs(h(:,line, :)), [],  1).* ...
                conj(fft(abs(flip(h(:, line+n_line/2, :))), [],  1))));
            pha_g_x = unwrap(angle(g_x));
            sup_g_x = abs(g_x) >= max(abs(g_x))./10;
%             sup_idx = sup_g_x;
         for part=1:n_part
            k = diff(pha_g_x(sup_g_x(:,part), part), 1, 1);
            shifts(line, part) = -mean(k, 1)*n_points/(2*pi) - 1;
         end
    end
    shifts = [shifts; -shifts];
    figure;imagesc(shifts)
end

% k_tra_my = zeros(3, sample_number, line_number);
% k_tra_my(1, :,:) = real(k_tra'); k_tra_my(2, :,:) = imag(k_tra');
% k_tra_my = single(k_tra_my*510);
% 
% 
% % k_tra_bart = bart('traj -r -D -c -x512 -y256');
% % k_tra_bart = k_tra_bart(:,257:end,:);
% for k = 1:10
%     bart('estdelay', k_tra_my(:,:,:), reshape(h(:,:, k), [1, 256, 256]));
% end

% k_tra_corr = bart('traj -r -D -c -x512 -y256 -q-1.930189:-1.916843:-0.219811');
% k_tra_corr = k_tra_corr(:,257:end,:);



