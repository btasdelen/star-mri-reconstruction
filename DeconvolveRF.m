function M_k = DeconvolveRF(s_t, w1, threshold, method)
    % DECONVOLVERF Deconvolve w_1(t) from s(t)
    %   s_t       : Convoluted function
    %   w1        : Function to deconvolve s_t
    %   threshold : For fourier, it determines indices that will be zeroed
    %   according : to magnitude of w1. For wiener, it is noise power
    %   method    : "fourier", "wiener" or "conj-transpose"
    % Authored by Bilal Tasdelen
    [sample_number, line_number, partition_number] = size(s_t);
   
    if strcmp(method, 'fourier')
        %% Reconstruction

        S_f = fft(s_t, 2*sample_number); % T.A.ms, -fmax -> fmax, df

        W1_f = fft(w1, 2*sample_number); % T.ms, -fmax -> fmax, df
        W1_f = W1_f./max(W1_f);

        %% F{M0(gamma*G*t)*u(t)}
        % Compute deconvolution
        H = S_f./(1i*W1_f);  % A, -fmax -> fmax, df
        mask = abs(W1_f) < threshold;
        H(mask, :, :) = abs(W1_f(mask)).*H(mask, :, :);
          
%         H(mask,:,:) = 0;
        % M_k_f = 0.5*(tanh((abs(W1_f) - threshold(2))*threshold(1)) + 1).*res;
        M_k = ifft(H); % A.kHz, 
        M_k = M_k(1:sample_number, :, :);

    elseif strcmp(method, 'wiener')

        S_f = fft(s_t, 2*sample_number); % T.A.ms, -fmax -> fmax, df

        W1_f = fft(w1, 2*sample_number); % T.ms, -fmax -> fmax, df
        W1_f = W1_f./max(W1_f);

        %% F{M0(gamma*G*t)*u(t)}
        % Compute deconvolution
%         S_f_m = mean(S_f.*conj(S_f), 1);
        S_f_m = (S_f.*conj(S_f));
        G_f = conj(W1_f).*S_f_m./(W1_f.*conj(W1_f).*S_f_m + threshold);
        H = -1i.*G_f.*S_f;
        M_k = ifft(H); % A.kHz, 
        M_k = M_k(1:sample_number, :, :);
        

    elseif strcmp(method, 'conj-transpose')
        W = convmtx(w1, sample_number);
        W = W(1:sample_number, 1:threshold);
        
        M_k = zeros(threshold, line_number, partition_number);

        for part=1:partition_number
            %    pW = (W'*W)\W';
            %    pW = pinv(W);
            %    dec_scan = W'*double(scan);
            M_k(:, :,part) = W(1:sample_number, 1:threshold)'*s_t(:,:,part);

            %     for line=1:line_number
            %     %   dec_scan(:, line) = lsqminnorm(W, scan(:, line)); 
            %     %   dec_scan(:, line) = pW*(scan(:,line)*-1i);  
            %     %   dec_scan(:, line)  =scan(:,line)\W;
            %     end
        end
    end
end