function win = tukey_win(N, alpha, n_delay)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% Partial regions
n = (0:1:(N-1)) + n_delay;

win = zeros(N,1);
an = alpha*(N-1)/2;

r1 = (n >= 0)                  & (n <  an);
r2 = (n >= an)                 & (n <= (N-1)*(1 - alpha/2));
r3 = (n > (N-1)*(1 - alpha/2)) & (n <= (N-1)/(1 - alpha/2));

win(r1) = (1 + cos(pi*(n(r1)/an - 1)))/2;
win(r2) = 1;
win(r3) = (1 + cos(pi*(n(r3)/an - 2/alpha + 1)))/2;

end

